--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

-- <function name="htc_compute_sweight_ha" schema="" src="functions/htc_compute_sweight_ha.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

CREATE FUNCTION htc_compute_sweight_ha(
    IN      cluster_id          integer[],
    IN      ldsity              double precision[],
    IN      sweight             double precision[],
    IN      ssize               integer,
    IN      verbose             boolean default False,
    OUT     total               double precision,
    OUT     var               double precision
)
    AS 'MODULE_PATHNAME'
    LANGUAGE C;

-- </function>

-- <function name="htc_version" schema="" src="functions/htc_version.sql">
--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

CREATE OR REPLACE FUNCTION htc_version(
	OUT version     character varying
) RETURNS character varying
AS 'MODULE_PATHNAME', 'get_version'
LANGUAGE C STRICT;

COMMENT ON FUNCTION htc_version(
	OUT version     character varying
)
IS 'Function returns the md5 checksum of htc.so sourcefile (htc.c).';

CREATE OR REPLACE FUNCTION htc_version_ok(
) RETURNS boolean
AS $$SELECT htc_version() = 'c2ba450b7e359fc8ce7be827756be08a  htc.c';$$
LANGUAGE SQL;

COMMENT ON FUNCTION htc_version_ok(
)
IS 'Function returns true if htc.so version is correct for extension version.';

-- </function>
