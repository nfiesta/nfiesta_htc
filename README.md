# about

PostgreSQL extension containing user-defined aggregate function implementing Horvitz-Thompson estimator for continuous populations.

# Installation

Following general instructions for manual installation ([bare-metal](https://en.wikipedia.org/wiki/Bare-metal_server)) should be similar for any Linux.
Please, see specific commands used for automatic installation on Debian Buster using Docker. You can find corresponding comments in [Dockerfile](https://gitlab.com/nfiesta/nfiesta_devenv/blob/master/Dockerfile):

```bash
# fully upgrade system
# set locales to be able to use UTF-8
# create your linux user (we will use user vagrant) and set password
# set up sudo, add your user to sudoers group and allow to act as root and postgres user without asking password
# install: make, gcc, git, wget, gnupg, ca-certificates
# add [pgdg](https://www.postgresql.org/download/linux/debian/) repository
# install latest postgres & postgis, corresponding postgresql-server-dev, plpython3 and python3-numpy
```

folowed by [.gitlab-ci.yml](https://gitlab.com/nfiesta/nfiesta_htc/blob/master/.gitlab-ci.yml):

```bash
# build extension (compilation htc.so)
# start postgres
# create DB user (preferably with same name as Linux user to bypass password provision)
# install extension [htc](https://gitlab.com/nfiesta/nfiesta_htc)
# run regression tests
```

REMARKS:
* build extension – means compile shared objects
* install extensin – means copy extension files (.control file and updatescripts) to PostgreSQL system directory.
* create extension – SQL command "CREATE EXTENSION ... ;" will run updatescripts from previous step and register objects belonging to the extensionj in DB
* run regression tests – optional step, it will create DB contrib\_regression,
run tests from sql directory and compare results with expected directory.
Command needs to be run with PostgreSQL superuser (to be able to drop and recreate contrib\_regression database).

## Create functionality on production DB

Last step is to create DB objects inside you running (production) DB. You can do it by commands:
```sql
CREATE EXTENSION postgis;
CREATE EXTENSION plpython3u;
CREATE EXTENSION htc;
```

# update extension htc to newer version

```bash
# get fresh source code (new update-scripts)
git pull
sudo make install
```

```sql
ALTER EXTENSION htc UPDATE TO ...;
```

# further documentation

* see regression tests in [htc.sql](https://gitlab.com/nfiesta/nfiesta_htc/blob/master/sql/htc.sql) and [htc_random.sql](https://gitlab.com/nfiesta/nfiesta_htc/blob/master/sql/htc_random.sql)

* https://wiki.postgresql.org/wiki/Regression_test_authoring
* http://www.postgresql.org/docs/devel/static/xfunc-c.html
* http://www.postgresql.org/docs/devel/static/extend-pgxs.html
* http://www.postgresql.org/docs/devel/static/extend-extensions.html
