MODULES = htc

EXTENSION = htc
DATA = htc--1.0.sql \
       htc--1.0--1.1.sql \
       htc--1.1--1.2.sql

PGFILEDESC = "htc - extension for computing estimates using Horvitz-Thompson Continuous theorem"

REGRESS = install \
	  so_version \
	  htc \
	  htc_random_data \
	  htc_random \

PG_CONFIG = pg_config
PGXS := $(shell $(PG_CONFIG) --pgxs)
EXTRA_CLEAN = version.h
include $(PGXS)

htc.o: FORCE

FORCE:
	echo "const char *md5version = \"$(shell md5sum htc.c)\";" > version.h
