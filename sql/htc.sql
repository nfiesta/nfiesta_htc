--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

with w_data as (
        select 
                ARRAY[1,2,3,4] as cluster_id,
                ARRAY[1.1, 2.2, 3.3, 4.4] as ldsity,
                ARRAY[0.75, 0.65, 0.55, 0.45] as sweight
)
, w_data_agg as (
        select 
                cluster_id as cluster_id_vec,
                ldsity as ldsity_vec,
                sweight as sweight_vec,
                123.456 as stratum_area,
                75 as stratum_sweight_sum,
                100 as ssize
        from w_data 
)
--------------------------------SQL implementation
, w_data_unnest as (
	select 
	unnest(cluster_id_vec) as cid,
	unnest(ldsity_vec) as ldsity,
	unnest(sweight_vec) as sweight,
	stratum_area, stratum_sweight_sum, ssize from w_data_agg
)
, w_data_pi as (
	select *, stratum_sweight_sum / (stratum_area * sweight) as pi from w_data_unnest
)
/*
select 1/pi as sweight_ha from w_data_pi
;
┌────────────────────────┐
│       sweight_ha       │
├────────────────────────┤
│ 1.23456000000000000000 │
│ 1.06995199999999999999 │
│ 0.90534400000000000634 │
│ 0.74073599999999997470 │
└────────────────────────┘
(4 rows)
*/
, w_sumA as (
	select sum((ldsity / pi)) as point, sum((ldsity / pi)^2) as sumA from w_data_pi 
)
, w_sumB as (
	select sum(
			data_x.ldsity * data_y.ldsity * 
			((
			(((data_x.ssize-1) * data_x.stratum_sweight_sum^2) / (data_x.ssize * data_x.stratum_area^2 * data_x.sweight * data_y.sweight))
				- data_x.pi * data_y.pi
			)/(
			(((data_x.ssize-1) * data_x.stratum_sweight_sum^2) / (data_x.ssize * data_x.stratum_area^2 * data_x.sweight * data_y.sweight))
				* data_x.pi * data_y.pi)
			)
		) as sumB
		from w_data_pi as data_x, w_data_pi as data_y where data_x.cid != data_y.cid
)
select 
	'SQL implementation' as implementation, 
	round(point::numeric, 10) as point, round((sumA + sumB)::numeric, 10) as var from w_sumA, w_sumB
union all
--------------------------------C implementation
select 
	'C implementation, htc_compute(...)' as implementation,
	round(total::numeric, 10) as point, round(var::numeric, 10) as var
	from (select (htc_compute(cluster_id_vec, ldsity_vec, sweight_vec, stratum_area, stratum_sweight_sum, ssize)).* from w_data_agg) as res;

----------------------------------------------------------------------absolute sampling weights (inverse of pi)
with w_data as (
	select
		ARRAY[1,2,3,4] as cluster_id,
		ARRAY[1.1, 2.2, 3.3, 4.4] as ldsity,
		ARRAY[1.23456000000000000000,1.06995199999999999999,0.90534400000000000634,0.74073599999999997470] as sweight_ha
)
, w_data_agg as (
	select
		cluster_id as cluster_id_vec,
		ldsity as ldsity_vec,
		sweight_ha as sweight_ha_vec,
		100 as ssize
	from w_data
)
--------------------------------SQL implementation
, w_data_unnest as (
	select
	unnest(cluster_id_vec) as cid,
	unnest(ldsity_vec) as ldsity,
	unnest(sweight_ha_vec) as sweight_ha,
	ssize as ssize
	from w_data_agg
)
, w_data_pi as (
	select *, (1 / sweight_ha) as pi from w_data_unnest
)
, w_sumA as (
	select sum((ldsity / pi)) as point, sum((ldsity / pi)^2) as sumA from w_data_pi
)
/*, w_data_pixy as (
	select data_x.cid, data_y.cid, data_x.ssize::float, data_x.pi, data_y.pi, (data_x.ssize::float-1) / (data_x.ssize) * data_x.pi * data_y.pi as pixy
	from w_data_pi as data_x, w_data_pi as data_y where data_x.cid != data_y.cid
)
select * from w_data_pixy*/
, w_sumB as (
	select sum(
			data_x.ldsity * data_y.ldsity *
			(
			((data_x.ssize::float-1) / (data_x.ssize) * data_x.pi * data_y.pi)
				- data_x.pi * data_y.pi
			)/(
			((data_x.ssize::float-1) / (data_x.ssize) * data_x.pi * data_y.pi)
				* data_x.pi * data_y.pi
			)
		) as sumB
		from w_data_pi as data_x, w_data_pi as data_y where data_x.cid != data_y.cid
)
select
	'SQL implementation with absolute sampling weights (inverse of pi)' as implementation,
	round(point::numeric, 10) as point, round((sumA + sumB)::numeric, 10) as var from w_sumA, w_sumB
union all
--------------------------------C implementation
select
	'C implementation, htc_compute_sweight_ha(...)' as implementation,
	round(total::numeric, 10) as point, round(var::numeric, 10) as var
	from (select (htc_compute_sweight_ha(cluster_id_vec, ldsity_vec, sweight_ha_vec, ssize)).* from w_data_agg) as res;
;
