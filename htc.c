//
// Copyright 2017, 2022 ÚHÚL
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

#include "postgres.h"
#include "fmgr.h"
#include "utils/builtins.h"
#include "utils/array.h"
#include "funcapi.h" // WHEN RETURNING RECORD
#include "access/htup_details.h" //when USING VERSION WITH DATUM

#include "version.h"

PG_MODULE_MAGIC;

PGDLLEXPORT Datum htc_compute(PG_FUNCTION_ARGS);
PG_FUNCTION_INFO_V1(htc_compute);

Datum
htc_compute(PG_FUNCTION_ARGS)
{
	ArrayType *vcid = PG_GETARG_ARRAYTYPE_P(0);
	int cid_dim = ARR_DIMS(vcid)[0];
	int *cid_data = (int *)ARR_DATA_PTR(vcid);

	ArrayType *vldsity = PG_GETARG_ARRAYTYPE_P(1);
	int ldsity_dim = ARR_DIMS(vldsity)[0];
	float8 *ldsity_data = (float8 *)ARR_DATA_PTR(vldsity);

	ArrayType *vsweight = PG_GETARG_ARRAYTYPE_P(2);
	int sweight_dim = ARR_DIMS(vsweight)[0];
	float8 *sweight_data = (float8 *)ARR_DATA_PTR(vsweight);

	float8 stratum_area = PG_GETARG_FLOAT8(3);
	float8 stratum_sweight_sum = PG_GETARG_FLOAT8(4);
	unsigned long ssize = PG_GETARG_INT32(5);
	bool verbose = PG_GETARG_BOOL(6);

	float8 count = 0;
	float8 pix = 0;
	float8 piy = 0;
	float8 pixy = 0;
	float8 sumA = 0;
	float8 sumB = 0;
	float8 point = 0;
	float8 var = 0;

	//-------------------MAKE RESULT AS RECORD
	TupleDesc tupdesc;
	Datum values[2];
	bool  nulls[2] = {false, false};
	HeapTuple    tuple;
	Datum        result;

	//-------------------CODE
	if ((cid_dim != ldsity_dim) || (cid_dim != sweight_dim))
		elog(ERROR, "vector size not equal %d %d %d", cid_dim, ldsity_dim, sweight_dim);

	for (int i = 0; i < ldsity_dim; i++)
	{
		if (verbose){
			elog(NOTICE, "processing cluster id %d. ldsity: %f, sweight: %f", cid_data[i], ldsity_data[i], sweight_data[i]);
		}
		pix = stratum_sweight_sum / (stratum_area * sweight_data[i]);
		sumA += (ldsity_data[i] / pix) * (ldsity_data[i] / pix);
		point += ldsity_data[i] / pix;

		for(int j = 0; j < ldsity_dim; j++)
		{
			if (i < j){
				piy = stratum_sweight_sum / (stratum_area * sweight_data[j]);
				pixy = ((ssize - 1) * stratum_sweight_sum * stratum_sweight_sum) 
					/ ( ssize * stratum_area * stratum_area * sweight_data[i] * sweight_data[j]);
				//implement HTC
				sumB += 2 * ldsity_data[i] * ldsity_data[j] * 
					((pixy - pix * piy) / (pixy * pix * piy));
				count += 1;
			}
		}
	}
	var = sumA + sumB;
	//PG_RETURN_FLOAT8(sumA + sumB);
	
        //-------------------MAKE RESULT AS RECORD
	/* Build a tuple descriptor for our result type */
	if (get_call_result_type(fcinfo, NULL, &tupdesc) != TYPEFUNC_COMPOSITE)
		ereport(ERROR, (errcode(ERRCODE_FEATURE_NOT_SUPPORTED), errmsg("function returning record called in context that cannot accept type record")));

	/* generate attribute metadata needed later to produce tuples from raw C strings*/
	tupdesc = BlessTupleDesc(tupdesc);

	values[0] = Float8GetDatum((float8)point);
	values[1] = Float8GetDatum((float8)var);

	/* build a tuple */
	tuple = heap_form_tuple(tupdesc, values, nulls);

	/* make the tuple into a datum */
	result = HeapTupleGetDatum(tuple);
	PG_RETURN_DATUM(result);
}

PGDLLEXPORT Datum htc_compute_sweight_ha(PG_FUNCTION_ARGS);
PG_FUNCTION_INFO_V1(htc_compute_sweight_ha);

Datum
htc_compute_sweight_ha(PG_FUNCTION_ARGS)
{
	ArrayType *vcid = PG_GETARG_ARRAYTYPE_P(0);
	int cid_dim = ARR_DIMS(vcid)[0];
	int *cid_data = (int *)ARR_DATA_PTR(vcid);

	ArrayType *vldsity = PG_GETARG_ARRAYTYPE_P(1);
	int ldsity_dim = ARR_DIMS(vldsity)[0];
	float8 *ldsity_data = (float8 *)ARR_DATA_PTR(vldsity);

	ArrayType *vsweight = PG_GETARG_ARRAYTYPE_P(2);
	int sweight_dim = ARR_DIMS(vsweight)[0];
	float8 *sweight_data = (float8 *)ARR_DATA_PTR(vsweight);

	unsigned long ssize = PG_GETARG_INT32(3);
	bool verbose = PG_GETARG_BOOL(4);

	float8 count = 0;
	float8 pix = 0;
	float8 piy = 0;
	float8 pixy = 0;
	float8 sumA = 0;
	float8 sumB = 0;
	float8 point = 0;
	float8 var = 0;

	//-------------------MAKE RESULT AS RECORD
	TupleDesc tupdesc;
	Datum values[2];
	bool  nulls[2] = {false, false};
	HeapTuple    tuple;
	Datum        result;

	//-------------------CODE
	if ((cid_dim != ldsity_dim) || (cid_dim != sweight_dim))
		elog(ERROR, "vector size not equal %d %d %d", cid_dim, ldsity_dim, sweight_dim);

	for (int i = 0; i < ldsity_dim; i++)
	{
		if (verbose){
			elog(NOTICE, "processing cluster id %d. ldsity: %f, sweight: %f", cid_data[i], ldsity_data[i], sweight_data[i]);
		}
		pix = 1.0 / sweight_data[i];
		sumA += (ldsity_data[i] / pix) * (ldsity_data[i] / pix);
		point += ldsity_data[i] / pix;

		for(int j = 0; j < ldsity_dim; j++)
		{
			if (i < j){
				piy = 1.0 / sweight_data[j];
				pixy = (((float8)ssize - 1) / ssize) * pix * piy;
				//implement HTC
				sumB += 2 * ldsity_data[i] * ldsity_data[j] *
					((pixy - pix * piy) / (pixy * pix * piy));
				count += 1;
			}
		}
	}
	var = sumA + sumB;
	//PG_RETURN_FLOAT8(sumA + sumB);
	
        //-------------------MAKE RESULT AS RECORD
	/* Build a tuple descriptor for our result type */
	if (get_call_result_type(fcinfo, NULL, &tupdesc) != TYPEFUNC_COMPOSITE)
		ereport(ERROR, (errcode(ERRCODE_FEATURE_NOT_SUPPORTED), errmsg("function returning record called in context that cannot accept type record")));

	/* generate attribute metadata needed later to produce tuples from raw C strings*/
	tupdesc = BlessTupleDesc(tupdesc);

	values[0] = Float8GetDatum((float8)point);
	values[1] = Float8GetDatum((float8)var);

	/* build a tuple */
	tuple = heap_form_tuple(tupdesc, values, nulls);

	/* make the tuple into a datum */
	result = HeapTupleGetDatum(tuple);
	PG_RETURN_DATUM(result);
}

//---------------------------------------get version--------------------------
PG_FUNCTION_INFO_V1(get_version);
Datum
get_version(PG_FUNCTION_ARGS) {
	int len = strlen(md5version);
	text       *result = (text *) palloc(len + VARHDRSZ);
	SET_VARSIZE(result, len + VARHDRSZ);
	memcpy(VARDATA(result), md5version, len);
	PG_RETURN_VARCHAR_P((VarChar *)result);
}
