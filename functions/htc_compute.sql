--
-- Copyright 2017, 2022 ÚHÚL
--
-- Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
-- You may not use this work except in compliance with the Licence.
-- You may obtain a copy of the Licence at:
--
-- https://joinup.ec.europa.eu/software/page/eupl
--
-- Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the Licence for the specific language governing permissions and limitations under the Licence.
--

CREATE FUNCTION htc_compute(
    IN      cluster_id          integer[],
    IN      ldsity              double precision[],
    IN      sweight             double precision[],
    IN      stratum_area        double precision,
    IN      stratum_sweight_sum double precision,
    IN      ssize               integer,
    IN      verbose             boolean default False,
    OUT     total               double precision,
    OUT     var               double precision
)
    AS 'MODULE_PATHNAME'
    LANGUAGE C;
